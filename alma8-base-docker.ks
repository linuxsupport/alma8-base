#version=RHEL8
text
skipx
network  --bootproto=dhcp
keyboard --vckeymap=us --xlayouts='us'
lang en_US.UTF-8
rootpw --iscrypted --lock locked
selinux --enforcing
firewall --disabled
# System timezone
timezone --utc Europe/Zurich
zerombr
clearpart --all --initlabel
part / --fstype ext4 --size=5000
reboot

%packages --excludedocs --nocore --instLangs=en --excludeWeakdeps
bash
binutils
-brotli
almalinux-release
CERN-CA-certs
cern-krb5-conf
cern-wrappers
coreutils-single
-dosfstools
-e2fsprogs
-firewalld
-fuse-libs
glibc-minimal-langpack
-gnupg2-smime
hostname
-iptables
iputils
-kernel
kexec-tools
less
-pinentry
-qemu-guest-agent
rootfiles
-shared-mime-info
tar
-trousers
vim-minimal
-xfsprogs
-xkeyboard-config
yum
%end

%pre
ARCH=`uname -m`
cat >> /etc/rsyslog.conf  <<DELIM
\$template AnacondaTemplate, "<%PRI%>%TIMESTAMP:::date-rfc3339% image:alma8-base-docker-$ARCH %syslogtag:1:32%%msg:::sp-if-no-1st-sp%%msg%"
\$ActionForwardDefaultTemplate AnacondaTemplate

module(load="imfile" mode="inotify")
input(type="imfile"
  File="/tmp/anaconda.log"
  Tag="anaconda")
input(type="imfile"
  File="/tmp/dnf.librepo.log"
  Tag="dnf-librepo")
input(type="imfile"
  File="/tmp/packaging.log"
  Tag="packaging")
input(type="imfile"
  File="/tmp/ks-script-*.log"
  Tag="ks-pre")
input(type="imfile"
  File="/mnt/sysroot/root/ks-post.log"
  Tag="ks-post")

*.* @@linuxsoftadm.cern.ch:5014
DELIM
/usr/bin/systemctl restart rsyslog
%end

%post --log=/root/ks-post.log
rpm --import /etc/pki/rpm-gpg/*

# some packages get installed even though we ask for them not to be,
# and they don't have any external dependencies that should make
# anaconda install them
TOREMOVE="bind-export-libs dhcp-libs dhcp-client dhcp-common dracut-network ethtool file freetype gettext gettext-libs grub2-tools-minimal grub2-tools-extra grub2-common grub2-tools grubby iproute kexec-tools libcroco libgomp libmnl lzo os-prober snappy which"

for remove in $TOREMOVE; do echo -n "removing $remove: "; rpm -e --nodeps $remove; done

# Add tsflags to keep yum from installing docs
echo "tsflags=nodocs" >> /etc/yum.conf

# Add install_weak_deps to keep yum from installing weak dependencies
echo "install_weak_deps=False" >> /etc/yum.conf

# Make sure we're up to date
dnf -y distro-sync

LANG="en_US"
echo "%_install_langs $LANG" > /etc/rpm/macros.image-language-conf

rm -rf /etc/firewalld
rm -rf /boot
rm -f /etc/udev/hwdb.bin
rm -rf /usr/lib/udev/hwdb.d/
rm -rf /boot
rm -rf /var/lib/dnf/history.*

#delete a few systemd things
rm -rf /etc/machine-id

#Make it easier for systemd to run in Docker container
cp /usr/lib/systemd/system/dbus.service /etc/systemd/system/
sed -i 's/OOMScoreAdjust=-900//' /etc/systemd/system/dbus.service

#Mask mount units and getty service so that we don't get login prompt
systemctl mask systemd-remount-fs.service dev-hugepages.mount sys-fs-fuse-connections.mount systemd-logind.service getty.target console-getty.service


# Generate install-time file record
cat << EOF > /etc/BUILDTIME
AlmaLinux 8 - CERN edition
Built on: `date`

 ⠀⠀⠀⠀⠀⠀⢱⣆⠀⠀⠀⠀⠀⠀
 ⠀⠀⠀⠀⠀⠀⠈⣿⣷⡀⠀⠀⠀⠀
 ⠀⠀⠀⠀⠀⠀⢸⣿⣿⣷⣧⠀⠀⠀
 ⠀⠀⠀⠀⡀⢠⣿⡟⣿⣿⣿⡇⠀⠀
 ⠀⠀⠀⠀⣳⣼⣿⡏⢸⣿⣿⣿⢀⠀
 ⠀⠀⠀⣰⣿⣿⡿⠁⢸⣿⣿⡟⣼⡆
 ⢰⢀⣾⣿⣿⠟⠀⠀⣾⢿⣿⣿⣿⣿
 ⢸⣿⣿⣿⡏⠀⠀⠀⠃⠸⣿⣿⣿⡿
 ⢳⣿⣿⣿⠀⠀⠀⠀⠀⠀⢹⣿⡿⡁
 ⠀⠹⣿⣿⡄⠀⠀⠀⠀⠀⢠⣿⡞⠁
 ⠀⠀⠈⠛⢿⣄⠀⠀⠀⣠⠞⠋⠀⠀
 ⠀⠀⠀⠀⠀⠀⠉⠀⠀⠀⠀⠀⠀⠀
EOF

#  man pages and documentation
find /usr/share/{man,doc,info,gnome/help} \
        -type f | xargs /bin/rm

#  ldconfig
rm -rf /etc/ld.so.cache
rm -rf /var/cache/ldconfig/*
rm -rf /var/cache/dnf/*
rm -f /tmp/ks-script*

rm -f /usr/lib/locale/locale-archive

#Setup locale properly
localedef -v -c -i en_US -f UTF-8 en_US.UTF-8

# Clean up after the installer.
rm -f /etc/rpm/macros.imgcreate

# Fix /run/lock breakage since it's not tmpfs in docker
umount /run
systemd-tmpfiles --create --boot

# Don't use KEYRING ccache for Kerberos, doesn't work well in docker
sed -i '/default_ccache_name.*KEYRING/d' /etc/krb5.conf
%end

%post --nochroot
# Make rsyslog send everything before the reboot
pkill -HUP rsyslogd
sleep 30s
rm -rf /mnt/sysroot/root/ks-post.log
%end
