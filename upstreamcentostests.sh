#!/bin/bash
# The following script runs Upstream CentOS functional tests with some tweaks for containerized context

yum install git jq iproute patch -y

# Find the last commit that passed upstream's Jenkins
PASSING=$(curl -s https://ci.centos.org/job/CentOS-Core-QA-t_functional-c8-stream-64/lastSuccessfulBuild/api/json | jq -r '.actions[] | select(.lastBuiltRevision) | .lastBuiltRevision.SHA1')
PASSING=${PASSING:-master}

git clone https://gitlab.cern.ch/linuxsupport/centos_functional_tests.git
cd centos_functional_tests
BRANCH=$(git reset --hard $PASSING)
echo -e "\e[1m\e[32m$BRANCH\e[0m"
BRANCH=$(git log -1)
echo -e "\e[1m\e[32m$BRANCH\e[0m"

# Skipping list is way longer than for cc7-base as image creation is different,
# cc7-base was systemd ready, meaning most of these skips are service related
cat > ./skipped-tests.list  <<DELIM
# This file contains list of tests we need/want to skip
# Reason is when there is upstream bug that we're aware of
# So this file should contain:
#  - centos version (using $centos_ver)
#  - test to skip (tests/p_${name}/test.sh)
#  - reason why it's actually skipped (url to upstream BZ, or bug report)
# Separated by |
8|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
8|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
8|tests/0_common/30_dns_works.sh|Cannot test, we specifically remove libunistring package
8|tests/0_common/50_test_comps.sh|No need to test this here
8|tests/p_arpwatch/10_arpwatch_test.sh|arpwatch always ends up in coredump in c8 but install is needed for posterior tests
8|tests/p_audit/*|auditd not yet container ready
8|tests/p_autofs/*|No NFS on containers
8|tests/p_bind/*|dig cannot find servers in c8 containers
8|tests/p_bridge-utils/*|Not possible in a container
8|tests/p_centos-release/*|Not actually CentOS
8|tests/p_coreutils/*|Specificially removed on KS file for lightweight container, installed coreutils-single instead
8|tests/p_cron/*|No systemd therefore cannot be tested
8|tests/p_diffutils/10-cmp-tests|https://bugzilla.redhat.com/show_bug.cgi?id=1732960
8|tests/p_dovecot/*|Won't work because can't log in locally
8|tests/p_exim/0-install_exim.sh|No systemd therefore cannot be tested
8|tests/p_freeradius/*|No systemd therefore cannot be tested
8|tests/p_grub2/01_grub2_secureboot_signed.sh|Incorrect certificate
8|tests/p_gzip/30-gzexe-test|https://apps.centos.org/kanboard/project/23/task/833
8|tests/p_httpd/*|httpd results in 403 for all requests, disabling for now
8|tests/p_initscripts/*|No systemd therefore cannot be tested
8|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
8|tests/p_iptraf/*|iptraf doesn't log any traffic
8|tests/p_kernel/*|No kernel therefore cannot be tested
8|tests/p_lftp/*|No systemd therefore cannot be tested
8|tests/p_libvirt/*|No systemd therefore cannot be tested
8|tests/p_logwatch/*|Default conf does not print anything, it will always fail, allegedly because our rpm removals
8|tests/p_lsb/lsb_release_brand_test.sh|Not actually CentOS
8|tests/p_lsof/10-test_lsof.sh|No systemd therefore cannot be tested
8|tests/p_mailman/*|No systemd therefore cannot be tested
8|tests/p_mod_python/mod_python_test.sh
8|tests/p_mtr/mtr_test.sh|Fails too often
8|tests/p_mysql/*|No systemd therefore cannot be tested
8|tests/p_net-snmp/*|No systemd therefore cannot be tested
8|tests/p_network/*|Cannot create VLAN in containers
8|tests/p_nfs/*|NFS not relevant in a containerised context
8|tests/p_nmap/*|Not working on centos:8 either
8|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
8|tests/p_openssh/*|sshd service cannot run. No systemd
8|tests/r_pdf/01_pdf-test.sh|Not actually CentOS
8|tests/p_php/20-php-mysql-test.sh|No systemd therefore cannot be tested
8|tests/p_php/25-php-mysql55-test.sh|No systemd therefore cannot be tested
8|tests/p_php_*/*_mariadb.sh|No systemd therefore cannot be tested
8|tests/p_podman/*|Don't try to test podman inside docker...
8|tests/p_postfix/*|No systemd therefore cannot be tested
8|tests/p_postgresql/*|No systemd therefore cannot be tested
8|tests/p_python/20-python-mysql-test.sh|Can't connect to localhost
8|tests/p_python/25-python-mysql55-test.sh|Can't connect to localhost
8|tests/p_python3-mod_wsgi/*|No systemd therefore cannot be tested, added after https://gitlab.cern.ch/linuxsupport/testing/centos_functional_tests/-/commit/ff5c8768d1213a886c451ed9363a77a363c68d23
8|tests/p_python38-mod_wsgi/*|No systemd therefore cannot be tested
8|tests/p_rsync/0-install-rsync.sh|No systemd therefore cannot be tested
8|tests/p_rsync/10-rsync-test.sh|No systemd therefore cannot be tested
8|tests/p_samba/*|No systemd therefore cannot be tested
8|tests/p_selinux/*|Not applicable in C8 containers
8|tests/p_sendmail/*|No systemd therefore cannot be tested
8|tests/p_shim/*|No bootloader involved, boot path is always deleted so there is no secureboot
8|tests/p_squid/*|squid seems to always fail, disabling for now
8|tests/p_squirrelmail/0-install_squirrelmail.sh|No systemd therefore cannot be tested
8|tests/p_spamassassin/*|Skip spamassassin tests
8|tests/p_strace/*|Not possible in a container
8|tests/p_syslog/*|No rsyslog installed, also not relevant on containers
8|tests/p_systemd/*|It uses auditd as an example which is not yet container ready
8|tests/p_tcpdump/*|Not possible in a container
8|tests/p_telnet/10-test_telnet.sh|No systemd therefore cannot be tested
8|tests/p_tftp-server/*|No systemd therefore cannot be tested
8|tests/p_vsftpd/*|No systemd therefore cannot be tested
8|tests/p_webalizer/*|No systemd therefore cannot be tested
8|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
8|tests/r_check_mod_packages/*|Does not apply for CCentOS
8|tests/r_lamp/*|No systemd therefore cannot be tested
8|tests/z_rpminfo/*|Does not apply in our case
DELIM

# Don't try to read external resources, we may not have network connection
patch -p0 --ignore-whitespace <<'DELIM'
--- tests/p_curl/curl_test.sh    2020-07-21 19:33:04.136367594 +0200
+++ tests/p_curl/curl_test.sh    2020-07-21 19:34:24.078892598 +0200
@@ -6,8 +6,8 @@


 if [ $SKIP_QA_HARNESS -eq 1 ]; then
-  CHECK_FOR="The CentOS Project"
-  URL="http://www.centos.org/"
+  CHECK_FOR="CERN"
+  URL="http://linux.cern.ch/"
 else
   CHECK_FOR="Index of /srv"
   URL="http://repo.centos.qa/srv/CentOS/"
DELIM
# Exit if we couldn't patch the tests
[[ $? -eq 0 ]] || exit 1

find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/ci.centos.org/linuxsoft.cern.ch/g"
find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/mirror.centos.org/linuxsoft.cern.ch\/centos/g"
find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/centos-release/almalinux-release/g"

./runtests.sh
