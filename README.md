# AlmaLinux 8 Docker images

http://cern.ch/linux/almalinux/

## What is AlmaLinux 8 (ALMA8) ?

Upstream AlmaLinux 8, with minimal patches to integrate into the CERN computing environment

### Image building

```
koji image-build alma8-docker-base `date "+%Y%m%d"` alma8-image-8x \
	http://linuxsoft.cern.ch/cern/alma/8/BaseOS/\$arch/os/ x86_64 aarch64 \
	--ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/alma8-base#master \
	--kickstart=alma8-base-docker.ks --ksversion=RHEL8 --distro=RHEL-8.2 --format=docker \
	--factory-parameter=dockerversion 1.10.1 \
	--factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
	--factory-parameter=docker_cmd '["/bin/bash"]' \
	--factory-parameter=generate_icicle False \
	--wait --scratch
```
